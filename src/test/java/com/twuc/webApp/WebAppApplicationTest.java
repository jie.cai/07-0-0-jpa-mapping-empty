package com.twuc.webApp;

import com.twuc.webApp.dao.OfficeRepository;
import com.twuc.webApp.entity.Office;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest(showSql = false)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
class WebAppApplicationTest {

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    void hello_world() {
        assertTrue(true);
    }

    @Test
    void should_save_office() {
        Office savedOffice = officeRepository.save(new Office(1L, "HangKong"));
        Office otherOffice = officeRepository.save(new Office(2L, "HangKong"));
        entityManager.flush();
        assertNotNull(savedOffice);
        assertNotNull(otherOffice);
    }

    @Test
    void should_persist_office() {
        entityManager.persist(new Office(1L, "HangKong"));
        Office office = entityManager.find(Office.class, 1L);

        assertEquals("HangKong", office.getCity());
    }

    @Test
    void should_throw_exception_when_city_empty() {
        Office office = new Office(233L, null);

        assertThrows(Exception.class, () -> {
            entityManager.persist(office);
            entityManager.flush();
        });
    }

    @Test
    void should_throw_exception_when_city_name_over_30_words() {
        assertThrows(Exception.class, () -> {
            entityManager.persist(new Office(444L, "should_throw_exception_when_city_name_over_30_words"));
            entityManager.flush();
        });
    }
}
