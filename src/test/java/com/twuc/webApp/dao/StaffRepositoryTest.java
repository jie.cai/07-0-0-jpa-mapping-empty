package com.twuc.webApp.dao;

import com.twuc.webApp.entity.Staff;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest(showSql = false)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
class StaffRepositoryTest {
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private StaffRepository staffRepository;

    @Test
    void should_save_staff() {
        Staff staff = staffRepository.save(new Staff(1L, "hello", "world"));

        assertEquals("hello", staff.getName().getFirstName());
    }
}
